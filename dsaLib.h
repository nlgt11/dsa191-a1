/* Created by Nguyen Duc Dung on 2019-09-03.
 * =========================================================================================
 * Name        : dsaLib.h
 * Author      : Duc Dung Nguyen
 * Email       : nddung@hcmut.edu.vn
 * Copyright   : Faculty of Computer Science and Engineering - HCMUT
 * Description : The data structure library for Assignment 1
 * Course      : Data Structure and Algorithms - Fall 2019
 * =========================================================================================
 */

#ifndef DSA191_A1_DSALIB_H
#define DSA191_A1_DSALIB_H
#include <string>
#include <stdio.h>
#include <string.h>
#include <iostream>

using namespace std;

class DSAException {
    int     _error;
    string  _text;
public:

    DSAException() : _error(0), _text("Success") {}
    DSAException(int err) : _error(err), _text("Unknown Error") {}
    DSAException(int err, const char* text) : _error(err), _text(text) {}

    int getError() { return _error; }
    string& getErrorText() { return _text; }
};

template <class T>
struct L1Item {
    T data;
    L1Item<T> *pNext;
    L1Item() : pNext(NULL) {}
    L1Item(T &a) : data(a), pNext(NULL) {}
};

template <class T>
class L1List {
    L1Item<T>   *_pHead;// The head pointer of linked list
    size_t      _size;// number of elements in this list
public:
    L1List() : _pHead(NULL), _size(0) {}
    ~L1List();

    void    clean();
    bool    isEmpty() {
        return _pHead == NULL;
    }
    size_t  getSize() {
        return _size;
    }

    T&      at(int i);// give the reference to the element i-th in the list
    T&      operator[](int i);// give the reference to the element i-th in the list

    bool    find(T& a, int& idx);// find an element similar to a in the list. Set the found index to idx, set idx to -1 if failed. Return true if success.
    int     insert(int i, T& a);// insert an element into the list at location i. Return 0 if success, -1 otherwise
    int     remove(int i);// remove an element at position i in the list. Return 0 if success, -1 otherwise.

    int     push_back(T& a);// insert to the end of the list
    int     insertHead(T& a);// insert to the beginning of the list

    int     removeHead();// remove the beginning element of the list
    int     removeLast();// remove the last element of the list

    void    reverse();

    void    traverse(void (*op)(T&)) {
        // TODO: Your code goes here
        L1Item<T>* p = _pHead;
        while (p) {
            op(p->data);
            p = p->pNext;
        }
    }
    void    traverse(void (*op)(T&, void*), void* pParam) {
        // TODO: Your code goes here
    }
};

/// Insert item to the end of the list
/// Return 0 if success, -1 otherwise
template <class T>
int L1List<T>::push_back(T &a) {
    // TODO: Your code goes here
    L1Item<T>* pCur = _pHead;
    while(pCur->pNext != nullptr)
        pCur = pCur->pNext;
    pCur->pNext = new L1Item<T>(a);
    _size++;
    return 0;
}

/// Insert item to the front of the list
/// Return 0 if success, -1 otherwise
template <class T>
int L1List<T>::insertHead(T &a) {
    // TODO: Your code goes here
    L1Item<T>* pTemp = new L1Item<T>(a);
    pTemp->pNext = _pHead;
    _pHead = pTemp;
    pTemp = nullptr;
    delete pTemp;
    _size++;
    return 0;
}

/// Remove the first item of the list
/// Return 0 if success, -1 otherwise
template <class T>
int L1List<T>::removeHead() {
    // TODO: Your code goes here
    if(_size != 0) {
        L1Item<T>* pTemp = _pHead;
        _pHead = _pHead->pNext;
        delete pTemp;
        _size--;
        return 0;
    }
    else
        return -1;
}

/// Remove the last item of the list
/// Return 0 if success, -1 otherwise
template <class T>
int L1List<T>::removeLast() {
    // TODO: Your code goes here
    if (_size == 0)
        return -1;
    else {
        L1Item<p>* pPre = _pHead, pCur = pPre->pNext;
        while (pCur->pNext != nullptr) {
            pPre = pCur;
            pCur = pCur->pNext;
        }
        pPre->pNext = nullptr;
        delete pCur;
        _size--;
        return 0;
    }
}
//This is the end of the initial file, endif is at line 144

/// Give the reference to the element i-th in the list
template <class T>
T& L1List<T>::operator[](int index) {
    if (index < 0 || idx >= _size) throw DSAException(1, "Index out of range");
    L1Item<T>* pCur = _pHead;
    while(index) {
        pCur = pCur->pNext;
        index--;
    }
    return pCur->data;
}

/// Give the reference to the element i-th in the list
template <class T>
T& L1List<T>::at(int index) {
    if (index < 0 || idx >= _size) throw DSAException(1, "Index out of range");
    L1Item<T>* pCur = _pHead;
    while(index) {
        pCur = pCur->pNext;
        index--;
    }
    return pCur->data;  
}

/// Find an element similar to a in the list. 
/// Set the found index to idx, set idx to -1 if failed. Return true if success.
template <class T>
bool L1List<T>::find(T& a, int& idx) {
    idx = 0;
    L1Item<T>* pCur = _pHead;
    while (pCur != nullptr) {
        if (pCur->data == a) return true;
        pCur = pCur->pNext;
        idx++;
    }
    idx = -1;
    return false;
}

/// Remove an element at position i in the list.
/// Return 0 if success, -1 otherwise.
template <class T>
int L1List<T>::remove(int i) {
    if (_size == 0) return -1;
    if(i = 0) {
        L1List->removeHead();
        return 0;
    }
    if (i = _size - 1) {
        L1List->removeLast();
        return 0;
    }
    if (i < 0 || i > _size - 1) return -1;

    L1Item<T>* pPre = _pHead, pCur = pPre->pNext;
    while (i > 1) {
        pPre = pCur;
        pCur = pCur->pNext;
    }
    pPre->pNext = pCur->pNext;
    delete pCur;
    _size--;
    return 0;
}

/// Insert an element into the list at location i.
/// Return 0 if success, -1 otherwise
/// If index is larger than size, insert to last.
template <class T>
int L1List<T>::insert(int i, T& a) {
    if (i == 0) {
        L1Item<T>* pTemp = _pHead;
        _pHead = new L1Item<T>(a);
        _pHead->pNext = pTemp;
        pTemp = nullptr;
        delete pTemp;
        _size++;
        return 0;
    }
    if (i > _size) i = _size;
    L1Item<T>* pPre = _pHead, pCur = pPre->pNext;
    while (idx > 1) {
        pPre = pCur;
        pCur = pCur->pNext;
        idx--;
    }
    pPre->pNext = new L1Item<T>(a);
    pPre->pNext->pNext = pCur;
    _size++;
    return 0;
}
#endif //DSA191_A1_DSALIB_H
